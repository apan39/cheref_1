/*Meteor.startup(function () {
    console.log("gura");
    if (Books.find().count() === 0) {
        var books = [
            {title: "Discover Meteor", author: "TomColemanandSachaGrief", url: "https://www.discovermeteor.com/"},
            {title: "Your First Meteor Application", author: "David Turnbull", url: "http://meteortips.com/first-meteor-tutorial/"},
            {title: "Meteor Tutorial", author: "Matthew Platts", url: "http://www.meteor-tutorial.org/"},
            {title: "Meteor in Action", author: "Stephen Hochaus and Manuel Schoebel", url: "http://www.meteorinaction.com/"},
            {title: "Meteor Cookbook", author: "Abigail Watson", url: "https://github.com/awatson1978/meteor-cookbook/blob/master/table-of-contents.md"}
        ];
        _.each(books, function (book) {
            Books.insert(book);
        });
    }
});*/


if (Cheatsheets.find().count() === 0) {
    console.log("Creating records");
   var returnInserted = Cheatsheets.insert({
        "cheatsheet_pages": [{
            "templateId": "Template1",
            "name": "cheatsheet1_page1",
            "owner": "userId",
            "_id": "string",
            "rank": "number"
        }],
        "page_columns": [{
            "_id": "string",
            "parent_id": "string",
            "name": "cheatsheet1_page1_column1",
            "css": {},
            "html": {"width": "col-sm-4"},
            "rank": "3"
        }, {
            "parent_id": "string",
            "name": "cheatsheet1_page1_column2",
            "_id": "string",
            "rank": "2",
            "css": {},
            "html": {"width": "col-sm-4"}
        }, {
            "parent_id": "string",
            "name": "cheatsheet1_page1_column3",
            "_id": "string",
            "rank": "1",
            "css": {},
            "html": {"width": "col-sm-4"}
        }],
        "column_containers": [{
            "parent_id": "string",
            "name": "cheatsheet1_page1_column3_container1",
            "_id": "string",
            "rank": "number"
        }],
        "container_contentItems": [{
            "parent_id": "string",
            "_id": "string",
            "content": "String",
            "name": "cheatsheet1_page1_column1_container1_contentitem1",
            "rank": "number"
        }],
        "owner": "userId",
        "userId": "String",
        "name": "Cheatsheet1",
        "type":"template",
        "theme":"guraguratheme"
    })
    console.log("Record(s) with following id(s) created ", returnInserted);
}