Meteor.methods({

    'addCheatsheet': function (name) {
        return Cheatsheets.insert({name: name});
    },

    'newCheatsheet': function () {
        var cheatsheetTemplate = Cheatsheets.findOne({"name" : "Cheatsheet1"});
        var newFilteredCheatsheet = _.pick(cheatsheetTemplate, 'cheatsheet_pages', 'owner', 'userId', 'name', 'type');
        newFilteredCheatsheet.name = "New unsaved template";// this should be in the template

        var cheatsheetPageId = newFilteredCheatsheet.cheatsheet_pages[0]._id = (new Mongo.ObjectID())._str;
        var newCheatsheetId = Cheatsheets.insert(newFilteredCheatsheet); // let Mongo create the id

        newFilteredCheatsheet.cheatsheet_pages[0].parent_id = newCheatsheetId;
        newFilteredCheatsheet.cheatsheet_pages[0].cheatsheet_id = newCheatsheetId;

        var newCheatsheetPage = newFilteredCheatsheet.cheatsheet_pages[0];

        var newColumns = _.pick(cheatsheetTemplate, "page_columns"); //get only the columns
        var columnRank =0;
        var newestColumns = _.map(newColumns.page_columns, function (pageColumn) {
            pageColumn.cheatsheet_id = newCheatsheetId;
            pageColumn.parent_id = cheatsheetPageId;
            pageColumn._id = (new Mongo.ObjectID())._str;
            pageColumn.rank = columnRank++;
            return pageColumn
        });

        //set the page parent_id
        Cheatsheets.update({'_id': newCheatsheetId, "cheatsheet_pages._id": cheatsheetPageId},
            {$set: {'cheatsheet_pages.$': newCheatsheetPage}});

        Cheatsheets.update({'_id': newCheatsheetId},
            {$set: {'page_columns': newestColumns}});

        return newCheatsheetId;
    },

    'updateCheatsheetName': function (id, name) {
        return Cheatsheets.update({_id: id}, {$set: {name: name}});
    },
    'updateCheatsheetDescription': function (id, description) {
        return Cheatsheets.update({_id: id}, {$set: {description: description}});
    },
    'updateCheatsheetTemplateId': function (id, templateId) {
        return Cheatsheets.update({_id: id}, {$set: {templateId: templateId}});
    },
    'updateCheatsheetTags': function (id, tags) {
        return Cheatsheets.update({_id: id}, {$set: {tags: tags}});
    },
    'updateCheatsheetLanguage': function (id, lang) {
        return Cheatsheets.update({_id: id}, {$set: {language: lang}});
    },
    'updateCheatsheetVersion': function (id, version) {
        return Cheatsheets.update({_id: id}, {$set: {version: version}});
    },
    'removeCheatsheet': function (id) {
        return Cheatsheets.remove({_id: id});
    },
    'updatePageColumnAmount': function (currentColumns, cid) {
        Cheatsheets.update({_id:cid}, {$set:{'page_columns':currentColumns}});
    },
    'updatePageformat': function (cid, width) {
        Cheatsheets.update({_id:cid}, {$set:{'cheatsheet_width':width}});
    },
    'newContainer': function (parentId, cheatsheetId, rank) {
        var oldTemplate =  Cheatsheets.findOne({"name" : "Cheatsheet1"});
        var containersTemplate = _.pick(oldTemplate, 'column_containers');
        var containerObject = containersTemplate.column_containers[0]; // set default container
        containerObject._id = (new Mongo.ObjectID())._str; // set new id
        containerObject.parent_id = parentId; //set column id
        containerObject.cheatsheet_id = cheatsheetId; //set cheatsheet id
        containerObject.rank = rank;

        return Cheatsheets.update({'_id':containerObject.cheatsheet_id},
            {$addToSet:{'column_containers':containerObject}});
    },
    'newContentItem': function (parentId, cheatsheetId, rank, type) {
        var oldTemplate =  Cheatsheets.findOne({"name" : "Cheatsheet1"});
        var contentItemTemplate = _.pick(oldTemplate, 'container_contentItems');
        var contentItemObject = contentItemTemplate.container_contentItems[0]; // set default container
        contentItemObject._id = (new Mongo.ObjectID())._str; // set new id
        contentItemObject.parent_id = parentId; //set column id
        contentItemObject.cheatsheet_id = cheatsheetId; //set cheatsheet id
        contentItemObject.rank = rank;
        contentItemObject.type = type;
        console.log(contentItemObject);

        return Cheatsheets.update({'_id':contentItemObject.cheatsheet_id},
            {$addToSet:{'container_contentItems':contentItemObject}});


    },
    'deleteContainer': function (containerProps) {
        return Cheatsheets.update({_id: containerProps.cheatsheetId}, {$pull:{'column_containers':{"_id":containerProps.containerId},'container_contentItems':{"parent_id":containerProps.containerId}}});
    },
    'updateContainer': function (cheatsheet_id, column_container_id, newrank, parentId) {
        return Cheatsheets.update({'_id':cheatsheet_id, 'column_containers._id':column_container_id}, {$set: {'column_containers.$.rank': newrank, 'column_containers.$.parent_id':parentId}});
    },
    'updateContentItem': function (cheatsheet_id, container_contentitem_id, newrank, parentId) {
        return Cheatsheets.update({'_id':cheatsheet_id, 'container_contentItems._id':container_contentitem_id}, {$set: {'container_contentItems.$.rank': newrank, 'container_contentItems.$.parent_id':parentId}});
    },
    'deleteContentItem': function (contentItemProps) {
    return Cheatsheets.update({_id: contentItemProps.cheatsheetId}, {$pull:{'container_contentItems':{"_id":contentItemProps.contentItemId}}});
    }
});