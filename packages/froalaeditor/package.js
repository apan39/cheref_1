Package.describe({
  name: "apan39:froalaeditor",
  summary: "A beautiful jQuery WYSIWYG HTML rich text editor.",
  version: '2.0.0'

});

Package.onUse(function(api) {
  api.use([
    "jquery@1.11.0",
    "fortawesome:fontawesome@4.4.0"
  ], "client");

  api.addFiles([
    // CSS
    "css/froala_editor.min.css",
    "css/froala_style.min.css",

    // JS
    "js/froala_editor.min.js",

    // JS Plugins
    "js/plugins/align.min.js",
    "js/plugins/char_counter.min.js",
    "js/plugins/code_view.min.js",
    "js/plugins/colors.min.js",
    "js/plugins/emoticons.min.js",
    "js/plugins/entities.min.js",
    "js/plugins/file.min.js",
    "js/plugins/font_family.min.js",
    "js/plugins/font_size.min.js",
    "js/plugins/fullscreen.min.js",

    "js/plugins/inline_style.min.js",
    "js/plugins/line_breaker.min.js",
    "js/plugins/link.min.js",
    "js/plugins/lists.min.js",
    "js/plugins/paragraph_format.min.js",
    "js/plugins/paragraph_style.min.js",
    "js/plugins/quote.min.js",
    "js/plugins/save.min.js",
    "js/plugins/table.min.js",
    "js/plugins/url.min.js",
    "js/plugins/video.min.js"
  ], ["client"]);
});