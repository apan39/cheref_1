/**
 * Created by johanjeborn on 2015-11-02.
 */

/**
Feature: Convert mongoDb javascript objects to arrays
As a helper method
In order to serve a template each
 I need to be able to serve the each object with an array of objects
 AND
 DETECT attributes which holds multiple objects
 REMOVE the "parent" attribute name holding the objects
 WRAP each object within a new object
 INJECT the parent attribute name into each new object as name:parentAttributeName attribute
 EMBRACE the new objects in an array


 cheatsheet_id.container_contentitems.content.rid.value
 IF var children = cheatsheet_id.container_contentitems.content.rid.children
 ADD rid attribute to each child
    var childrenArray = _.toArray(children)
    EACH child cheatsheet_id.container_contentitems.content.rid.children.childid.value


Rules:
    - Column range 1..4
- Registered
- Logged in
- Warning when removing columns

Scenario: creating a table when adding a new contentitem
Given there are default tables with 1-4 columns
When I change the value(1--4) in the dropdown menu
Then I should have 1..4 columns in the table
--And the added columns should have been inserted after the existing columns

Retrieve a contentItem multirow from db:
GET the contentItem
CHECK the type
CHECK numberOfColumns
Sort the object
CONVERT object to Array
ITERATE array(rows) inside ContentItem template
SEND each row to subTemplate
BUILD the table and insert object keys at the right places
<tr id = "row_1"> <td id="_1_1">{{_1_1}}</td> <td id="_1_2">{{_1_2}}</td> </tr>
 */
