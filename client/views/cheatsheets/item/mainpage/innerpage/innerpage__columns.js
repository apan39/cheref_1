Template.innerPage.rendered = function() {
}


Template.innerPage.helpers({
    sorted_page_columns: function(){
        var sortedObjs = _.sortBy( Session.get("cheatsheet").page_columns, 'rank' );
        //var sortedObjs = _.sortBy( Template.parentData(1).page_columns, 'rank' ); //with iron router
        return sortedObjs;
    },
    cheatsheet_width: function(){
        var cheatsheet = Cheatsheets.findOne({'_id': Session.get("cheatsheet")._id});
        //var cheatsheet = Cheatsheets.findOne({'_id': Template.parentData(1)._id});//with iron router
        //console.log('templatewidth',Template.parentData(1).cheatsheet_width);
        return cheatsheet.cheatsheet_width;
    },

    templateId: function(){
        Session.set("template",this._id);
        return this.templateId;
    }

});

