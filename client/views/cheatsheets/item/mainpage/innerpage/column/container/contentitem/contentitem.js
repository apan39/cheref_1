Template.contentItem.helpers({
    /*selector: function () {
     return {title: "Discover Meteor"}; // this could be pulled from a Session var or something that is reactive
     },*/
    beforeSub: function(){
         console.log("aboveorbelow", Session.get('aboveOrBelowSelection'));
        if(Session.equals('aboveOrBelowSelection', 'jsNewBeforeContentItem')) {
            return Session.equals('thisId', this._id);
        }
    },    belowSub: function(){
        if(Session.equals('aboveOrBelowSelection', 'jsNewBelowContentItem')) {
            return Session.equals('thisId', this._id);
        }
    },
        sub_type: function(){
        var s_type = Session.get("contentItemType");
        return s_type;


        /*switch (Session.get("contentItemType")){
         case "multicolumn":
         console.log("chartTemplate");
         return true;
         break;
         case "code":
         return true;
         break;
         default:
         return false;
         }*/
    },

    sub_items: function(){
        //var a_rows = _.pairs(this.content);
        // convert property collection of objects to array of objects
        var a_rows = _.toArray(this.content);
        //console.log("toarray", _.toArray(this.content));
        //a_rows.sort();
        a_rows_sort = _.sortBy(a_rows, 'rank');
        return a_rows_sort;
    },

    sub_content_type: function () {
       // console.log("Rowtype", Template.parentData(1).type);
        return Template.parentData(1).type
    },
    contentt: function(){
        return this.content._1_1;
    },
    sorted_contentItems: function(){
        //var currentCheatsheet = Session.get('currentCheatsheet');
        //var sortedObjs = _.sortBy(currentCheatsheet.container_contentItems, 'rank' );
        var sortedObjs = [{'name':'game'}];
        return sortedObjs;
    },
    contentTypes: function(){
        return [{'name':'multicolumn', 'type':'column_number_menu'},
            {'name':'chart', 'type':'singlecolumn'}, {'name':'code', 'type':'singlecolumn'},
            {'name':'contentcolumn', 'type':'singlecolumn'}, {'name':'livecontent', 'type':'singlecolumn'},
            {'name':'mindmap', 'type':'singlecolumn'}, {'name':'onlyimage', 'type':'singlecolumn'},
            {'name':'outliner', 'type':'singlecolumn'}, {'name':'paragraph', 'type':'singlecolumn'},
            {'name':'quiz', 'type':'singlecolumn'}, {'name':'structural', 'type':'singlecolumn'},
            {'name':'table', 'type':'singlecolumn'}, {'name':'video', 'type':'singlecolumn'} ]
    },
    contenttype: function(){
        switch ("char") {
            case "chart":
                console.log("chartTemplate");
                return "chart";
                break;
            case "code":
                console.log("codeTemplate");
                break;
            case "contentcolumn":
                console.log("contentcolumnTemplate");
                break;
            case "livecontent":
                console.log("livecontentTemplate");
                break;
            case "mindmap":
                console.log("mindmapTemplate");
                break;
            case "onlyimage":
                console.log("onlyimageTemplate");
                break;
            case "outliner":
                console.log("outlinerTemplate");
                break;
            case "paragraph":
                console.log("paragraphTemplate");
                break;
            case "quiz":
                console.log("quizTemplate");
                break;
            case "structural":
                console.log("structuralTemplate");
                break;
            case "table":
                console.log("tableTemplate");
                break;
            case "video":
                console.log("videoTemplate");
                return "video";
                break;

            default:
                return 'chart';
        }
    }
});

Template.contentItem.events({
    'change .jsWhich_content_type': function (evt, tmpl) {
        /*   console.log("itemtypevalue", evt.target.value);
         var pickedNumberOfColumns = evt.target.value;
         Meteor.call('newContentItem', parentId, cheatsheetId, newRank, function(error, result){
         // Router.go('/cheatsheets/' + result);
         });*/
    },
    'click .jsContentItemButton, change .jsWhich_content_type': function(event, template){
        event.preventDefault();
       // event.stopPropagation();
        if(event.target.id !== 'jsNewBeforeContentItem'){
        $('#jsNewBeforeContentItem').prop('selectedIndex',0); //nullify dropbox
            $('#jsNewBeforeContentItem').attr("id",""); //nullify not longer active id
            $(event.target).attr("id","jsNewBeforeContentItem"); //set the current target(active dropbox) id
        }

        var type =event.target.value;
       // this.$(event.target).attr("id","rockon")
        console.log("id", $(event.target).data("shit"));
        //if($(event.target).data("id")== "jsNewBeforeContentItem"){
        //    $(event.target).attr("id","jsNewBeforeContentItem")
        //}
        //var type3 =$(event.target.options[0]);
        var type2 = $(event.target).find(':selected').data("subtype");
        type = type.replace(/'/g, '"');
        //type = JSON.parse(type);

        if (type2 == 'singlecolumn') {

            var parentId = Template.parentData(1)._id;
            var cheatsheetId = Session.get("cheatsheet")._id; //set cheatsheet id

            var thisObject = $('#' + template.data._id).get(0); //id på det aktuella containerobjektet
            var before = $('#' + template.data._id).prev().get(0);// tidigare containerobjekt
            var afterObject = $('#' + template.data._id).next().get(0);// direkt efter containerobjekt
            var newRank;
            if ((event.target.id == 'jsNewBeforeContentItem') || (event.target.id == 'jsNewTypeContentitemBefore')) {
               // console.log("jippibefore", before);
               // console.log("targetid", event.target.id);

                if (!before) { // moving to the top of the list
                    newRank = Blaze.getData(thisObject).rank - 1;
                } else {
                    newRank = (Blaze.getData(before).rank + Blaze.getData(thisObject).rank) / 2;
                }
            } else if ((event.target.id == 'jsNewBelowContentItem') || (event.target.id == 'jsNewTypeContentitemAfter')) {

                if (!afterObject) { // moving to the bottom of the list
                    newRank = Blaze.getData(thisObject).rank + 1;
                } else {
                    newRank = (Blaze.getData(thisObject).rank + Blaze.getData(afterObject).rank) / 2;
                }
            }

            //console.log(event.target.id); //get the id of the clicked button
            Meteor.call('newContentItem', parentId, cheatsheetId, newRank, type, function (error, result) {
                // Router.go('/cheatsheets/' + result);
            });
        }else {
            Session.set("contentItemType", type2)
            Session.set("thisId", this._id);
            Session.set('aboveOrBelowSelection', $(event.target).data("id"));
            $('#'+event.target.id).text();
            //$('#jsNewTypeContentitemBefore').prop('selectedIndex',0); nullify
            console.log("hallo",($('#'+event.target.id)[0]));
        }

    },
    'click #js-deleteContentItem': function(event, template){
        event.preventDefault();
        event.stopPropagation();
        var contentItemProps = {'contentItemId':this._id, 'cheatsheetId':this.cheatsheet_id}
        var deleteConfirmation = confirm("Really delete this contentitem?")
        if (deleteConfirmation){
            Meteor.call('deleteContentItem', contentItemProps, function(error, result){
                console.log(result);
            });
        }
    }//,
    //'focus .myEditor': function(event, template){
    /*
     if(Session.get(currentEditor)!= template.data._id ){
     var destroyThisEditor = '#' + Session.get(currentEditor) + ' .myEditor;
     $(destroyThisEditor).editable('destroy');

     }else{ include the code beneath}*/

    /*Session.set("currentEditor", template.data._id);
     var queryPath ='#'+template.data._id +' .myEditor';
     console.log(queryPath);

     $(queryPath).froalaEditor()({
     toolbarInline:true,
     //initOnClick: true,
     toolbarButtons: ['undo', 'redo' , '|', 'bold', 'italic', 'underline']
     })
     console.log(event.currentTarget);*/
    //}
});


Template.contentItem.onRendered(function () {
    /* $('.myEditor').froalaEditor()({

     })*/

    // Make table columns resizable TODO Create drag handle
    $("table.js-tc_resize").colResizable();
    // Make table rows sortable/draggable within and between table/tables
    $("tbody.js-tr_sort").sortable({
        connectWith: ".js-tr_sort"
    });
});


/* shows froala inline mode when clicking inside area
 toolbarInline: true,
 charCounterCount: false,
 toolbarButtons: ['bold', 'italic', 'underline', 'strikeThrough', 'subscript', 'superscript', '-', 'paragraphFormat', 'align', 'formatOL', 'formatUL', 'indent', 'outdent', '-', 'insertImage', 'insertLink', 'insertFile', 'insertVideo', 'undo', 'redo'],
 alwaysVisible: true*/
