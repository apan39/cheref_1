Template.container.helpers({
    sorted_contentItems: function(id, cheatsheetId){
        if(id) {
            //var cheatsheet = Cheatsheets.findOne({'_id': cheatsheetId});
            var contentItems = Session.get("cheatsheet").container_contentItems;
            //var contentItems = Template.parentData(3).container_contentItems;//iron router

            var filteredContentItems = _.filter(contentItems, function (contentItem) {
                return contentItem.parent_id == id;
            });
            var sortedContentItems = _.sortBy(filteredContentItems, 'rank');
            //var sortedContentItemsIsEmpty = _.isEmpty(sortedContentItems);
            //var objs = Cheatsheets.find({'_id': this.cheatsheet_id, "column_containers.parent_id":id})

            return sortedContentItems;
        }
    }
});

Template.container.events({
    'click .js-containerButton': function(event, template){
        event.preventDefault();
        event.stopPropagation();
        console.log('templatedataidthisobject',$('#'+template.data._id).get(0));
        console.log('templatedataidbeforeobject',$('#'+template.data._id).prev().get(0));

        var parentId = Template.parentData(1)._id;
        var cheatsheetId = Session.get("cheatsheet")._id; //set cheatsheet id
       // var cheatsheetId = Template.parentData(3)._id; //set cheatsheet id//iron router

        var thisObject = $('#'+template.data._id).get(0); //id på det aktuella containerobjektet
        var before = $('#'+template.data._id).prev().get(0);// tidigare containerobjekt
        var afterObject = $('#'+template.data._id).next().get(0);// direkt efter containerobjekt
        var newRank;
        if(event.target.id == 'js-newBeforeContainer'){
            if (!before) { // moving to the top of the list
                console.log('before')

                newRank = Blaze.getData(thisObject).rank-1;
            }else {
                newRank = (Blaze.getData(before).rank + Blaze.getData(thisObject).rank)/2;
            }
        } else if(event.target.id == 'js-newBelowContainer'){
            if (!afterObject) { // moving to the bottom of the list
                console.log('below')
                newRank = Blaze.getData(thisObject).rank + 1;
            } else {
                newRank = (Blaze.getData(thisObject).rank + Blaze.getData(afterObject).rank)/2;
            }
        }

        //console.log(event.target.id); //get the id of the clicked button
        Meteor.call('newContainer', parentId, cheatsheetId, newRank, function(error, result){
            // Router.go('/cheatsheets/' + result);
        });

    },
    'click #js-deleteContainer': function(event, template){
        event.preventDefault();
        event.stopPropagation();
        var containerProps = {'containerId':this._id, 'cheatsheetId':this.cheatsheet_id}
        var deleteConfirmation = confirm("Really delete this container?")
        if (deleteConfirmation){
            Meteor.call('deleteContainer', containerProps, function(error, result){
            });
        }
    },
    'click #js-newContentItem': function(event, template){
        event.preventDefault();
        event.stopPropagation();

        var parentId = this._id;
        var cheatsheetId = Session.get("cheatsheet")._id; //set cheatsheet id
       // var cheatsheetId = Template.parentData(3)._id; //set cheatsheet id //iron router
        if(template.$('.contentItem').length<1){
            var rank = 1
        }

        Meteor.call('newContentItem', parentId, cheatsheetId, rank, function(error, result){
            // Router.go('/cheatsheets/' + result);
        });
    }
});

Template.container.rendered = function(){


        // How to set parentId in sortable function before first drag
    this.$('.js-contentitemsort').sortable({ handle:".contentitem_handle",connectWith: ".js-contentitemsort",placeholder: "ui-state-highlight",
        stop: function (event, ui) { // fired when an item is dropped
            event.preventDefault();
            event.stopPropagation();

            var containerContentItem = ui.item.get(0);
            var before = ui.item.prev().get(0);
            var after = ui.item.next().get(0);
            var parentId = Blaze.getData(ui.item.parent().get(0))._id;

            var newRank;
            if (!before && after) { // moving to the top of the list
                newRank = Blaze.getData(after).rank - 1
            } else if (!after && before) { // moving to the bottom of the list
                newRank = Blaze.getData(before).rank + 1
            } else if (after && before){
                newRank = (Blaze.getData(before).rank + Blaze.getData(after).rank)/2;
            }else if (!after && !before){
                newRank = 1;
            }

            var cheatsheetId = Blaze.getData(containerContentItem).cheatsheet_id;
            var containerContentItemId = Blaze.getData(containerContentItem)._id;
            console.log(newRank)

            Meteor.call('updateContentItem', cheatsheetId, containerContentItemId ,newRank, parentId, function(error, result){
                //Router.go('/cheatsheets/' + result);
            });
        }
    });



}
