/**
 * Created by johanjeborn on 2015-12-01.
 */

Template.column_number_menu.events({

    'change .jsCol_num': function(event, template){
        event.preventDefault();
        // event.stopPropagation();
        if(event.target.id !== 'jsNewBeforeContentItem'){
            $('#jsNewBeforeContentItem').prop('selectedIndex',0); //nullify dropbox
            $('#jsNewBeforeContentItem').attr("id",""); //nullify not longer active id
            $(event.target).attr("id","jsNewBeforeContentItem"); //set the current target(active dropbox) id
        }

        var type =event.target.value;
        // this.$(event.target).attr("id","rockon")
        console.log("id", $(event.target).data("shit"));
        //if($(event.target).data("id")== "jsNewBeforeContentItem"){
        //    $(event.target).attr("id","jsNewBeforeContentItem")
        //}
        //var type3 =$(event.target.options[0]);
        var type2 = $(event.target).find(':selected').data("subtype");
        type = type.replace(/'/g, '"');
        //type = JSON.parse(type);

        if (type2 == 'singlecolumn') {

            var parentId = Template.parentData(1)._id;
            var cheatsheetId = Session.get("cheatsheet")._id; //set cheatsheet id

            var thisObject = $('#' + template.data._id).get(0); //id på det aktuella containerobjektet
            var before = $('#' + template.data._id).prev().get(0);// tidigare containerobjekt
            var afterObject = $('#' + template.data._id).next().get(0);// direkt efter containerobjekt
            var newRank;
            if ((event.target.id == 'jsNewBeforeContentItem') || (event.target.id == 'jsNewTypeContentitemBefore')) {
                // console.log("jippibefore", before);
                // console.log("targetid", event.target.id);

                if (!before) { // moving to the top of the list
                    newRank = Blaze.getData(thisObject).rank - 1;
                } else {
                    newRank = (Blaze.getData(before).rank + Blaze.getData(thisObject).rank) / 2;
                }
            } else if ((event.target.id == 'jsNewBelowContentItem') || (event.target.id == 'jsNewTypeContentitemAfter')) {

                if (!afterObject) { // moving to the bottom of the list
                    newRank = Blaze.getData(thisObject).rank + 1;
                } else {
                    newRank = (Blaze.getData(thisObject).rank + Blaze.getData(afterObject).rank) / 2;
                }
            }

            //console.log(event.target.id); //get the id of the clicked button
            Meteor.call('newContentItem', parentId, cheatsheetId, newRank, type, function (error, result) {
                // Router.go('/cheatsheets/' + result);
            });
        }else {
            Session.set("contentItemType", type2)
            Session.set("thisId", this._id);
            Session.set('aboveOrBelowSelection', $(event.target).data("id"));
            $('#'+event.target.id).text();
            //$('#jsNewTypeContentitemBefore').prop('selectedIndex',0); nullify
            console.log("hallo",($('#'+event.target.id)[0]));
        }

    },
    'click #js-deleteContentItem': function(event, template){
        event.preventDefault();
        event.stopPropagation();
        var contentItemProps = {'contentItemId':this._id, 'cheatsheetId':this.cheatsheet_id}
        var deleteConfirmation = confirm("Really delete this contentitem?")
        if (deleteConfirmation){
            Meteor.call('deleteContentItem', contentItemProps, function(error, result){
                console.log(result);
            });
        }
    }//,
    //'focus .myEditor': function(event, template){
    /*
     if(Session.get(currentEditor)!= template.data._id ){
     var destroyThisEditor = '#' + Session.get(currentEditor) + ' .myEditor;
     $(destroyThisEditor).editable('destroy');

     }else{ include the code beneath}*/

    /*Session.set("currentEditor", template.data._id);
     var queryPath ='#'+template.data._id +' .myEditor';
     console.log(queryPath);

     $(queryPath).froalaEditor()({
     toolbarInline:true,
     //initOnClick: true,
     toolbarButtons: ['undo', 'redo' , '|', 'bold', 'italic', 'underline']
     })
     console.log(event.currentTarget);*/
    //}
});
