
Template.column.helpers({
        sorted_column_containers: function(parentId, cheatsheetId){
            if(parentId) {
               // var cheatsheet = Cheatsheets.findOne({'_id': cheatsheetId});
               var columnContainers = Session.get("cheatsheet").column_containers;
               //var columnContainers = Template.parentData(2).column_containers; /iron router
                var filteredContainers = _.filter(columnContainers, function (container) {
                    return container.parent_id == parentId;
                });
                var sortedContainers = _.sortBy(filteredContainers, 'rank');
                var sortedContainersIsEmpty = _.isEmpty(sortedContainers);
                //var objs = Cheatsheets.find({'_id': this.cheatsheet_id, "column_containers.parent_id":id})

                return sortedContainers;
            }
        }

});

Template.column.rendered = function(){
    // How to set parentId in sortable function before first drag
    this.$(".csort").sortable({ handle:'.container_handle', connectWith: ".csort",
        stop: function (event, ui) { // fired when an item is dropped
            // event.preventDefault();
            // event.stopPropagation();

            var columnContainer = ui.item.get(0);
            var before = ui.item.prev().get(0);
            var after = ui.item.next().get(0);
            var parentId = Blaze.getData(ui.item.parent().get(0))._id;

            var newRank;
            if (!before && after) { // moving to the top of the list
                newRank = Blaze.getData(after).rank - 1
            } else if (!after && before) { // moving to the bottom of the list
                newRank = Blaze.getData(before).rank + 1
            } else if (after && before){
                newRank = (Blaze.getData(before).rank + Blaze.getData(after).rank)/2;
            }else if (!after && !before){
                newRank = 1;
            }

            var cheatsheetId = Blaze.getData(columnContainer).cheatsheet_id;
            var columnContainerId = Blaze.getData(columnContainer)._id;

            Meteor.call('updateContainer', cheatsheetId, columnContainerId ,newRank, parentId, function(error, result){
                //Router.go('/cheatsheets/' + result);
            });
        }
    });

    this.$(".sortablee").sortable({handle:".huppa"})
};

Template.column.events({
    //click button for new container
    'click #js-newContainer': function(event, template){
        event.preventDefault();
        event.stopPropagation();
console.log("clicked new container and this id=" + this._id )
        var parentId = this._id;
        var cheatsheetId = Session.get("cheatsheet")._id; //set cheatsheet id
        //var cheatsheetId = Template.parentData(2)._id; //set cheatsheet id //Iron router
        if(template.$('.js-container_item').length<1){//check if a container exists, if not set the rank to 1
            var rank = 1
        }

        Meteor.call('newContainer', parentId, cheatsheetId, rank, function(error, result){
            // Router.go('/cheatsheets/' + result);
        });
    }
});


