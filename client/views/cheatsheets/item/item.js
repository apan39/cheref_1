Template.item.rendered = function(){
    //console.log(this);
    var datas = this.data._id
    $.fn.editable.defaults.mode = 'inline';
    $('#textArea.editable').editable({
        success: function(response, newValue) {
            Cheatsheets.update(datas, {$set: {name: newValue}});
            }});

}

Template.item.events({
    'click #deleteCheatsheet': function(event, template){
        var deleteConfirmation = confirm("Really delete this cheatsheet?");

        if (deleteConfirmation){
            Meteor.call('removeCheatsheet', this._id, function(error, result){
            });
        }
    }
});

Template.item.helpers({
    isNotTemplate:function(){
        if (this.name!=="new_cheatsheet_template"){
            return true
        }

    }

})


