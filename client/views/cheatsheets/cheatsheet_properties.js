Template.cheatsheetProperties.onCreated(function() {
    var self = this;
    self.autorun(function() {
        var postId = FlowRouter.getParam('postId');
        // self.subscribe('singlePost', postId);
    });
});

Template.cheatsheetProperties.events({
    'click .js-editCheatsheetName': function (evt, tmpl) {
        console.log('thisworkingk', this._id)
        Session.set('editing_cheatsheetname', this._id);
        var dataId = '[data-id = ' + $(event.target).attr('data-id') + ']';
        Meteor.setTimeout(function () {
            $(dataId).focus();
        }, 250);
    },
    'change .js-name': function (event, tmpl) {
        if ("true"/*event.which === 27 || event.which === 13*/) {
            event.preventDefault();
            //console.log(tmpl.find('[data-id]'));
            var dataId = $(event.target).attr('data-id');
            var uCdataId = dataId.charAt(0).toUpperCase() + dataId.slice(1);//Capitalize first letter
            var ele = tmpl.find("[data-id=\"" + dataId + "\"]");
            Meteor.call('updateCheatsheet' + uCdataId, Session.get("cheatsheet")._id, $(event.target).val());
            Session.set('editing_CheatsheetName', null);
        }
    },
    'change .js-number_of_columns': function (evt, tmpl) {
        /*if (evt.target.value !== this.customer) {
         Meteor.call('updateProjectCustomer', Session.get('active_project'), evt.target.value);
         Session.set('editing_projectcustomer', false);
         }*/
        // console.log(evt.target.value);

        var pickedNumberOfColumns = evt.target.value; // Get the dropdown value
        var columnTemplate = _.clone(this.page_columns[0]); // A column template
        this.page_columns.length = 0; // Nullify current columns
        var currentColumns = this.page_columns;

        _(pickedNumberOfColumns).times(function(n){
            var aNewColumn = _.clone(columnTemplate);
            aNewColumn._id = (new Mongo.ObjectID())._str;
            aNewColumn.rank = n;
            if (12 % evt.target.value == 0) { // Checks if value is bootstrap column compatible, otherwise override in cheref.css

                aNewColumn.html.width = 'uk-width-large-' + (12 / evt.target.value) + '-12';//uk-width-large-6-12
            } else
            {
                aNewColumn.html.width = 'uk-width-large-'+ evt.target.value + '-12';
            }
            currentColumns.push(aNewColumn);

        });

/*
        var highestRankedObject =_.max(this.page_columns, function(column){
            return column.rank;
        });
        var highestRankedObjectNumber = highestRankedObject.rank;

        var currentColumns = this.page_columns;
        _.sortBy(currentColumns, 'rank'); // so the pop goes well
        var columnDiff = evt.target.value - currentColumns.length;
        if(columnDiff > 0) {
            _(columnDiff).times(function (n) {
                var templateColumn = _.clone(currentColumns[0]);
                templateColumn._id = (new Mongo.ObjectID())._str;
                templateColumn.rank = ++highestRankedObjectNumber;
                currentColumns.push(templateColumn);

                console.log(currentColumns)
                console.log( highestRankedObject)
            })
        }else if (columnDiff < 0 ){
            _(Math.abs(columnDiff)).times(function (n) {
                currentColumns.pop(); //pop the last ones
            })
        }

        if (12 % currentColumns.length == 0) {
            _.each(currentColumns, function (column) {
                column.html.width = 'col-sm-' + (12 / currentColumns.length);
            });
        } else
        {
            _.each(currentColumns, function(column){
                column.html.width = 'col-sm-'+evt.target.value;
            })
        }*/

        Meteor.call('updatePageColumnAmount', currentColumns, this._id, function (error, result) {
        });

        //set the width of the containers(currentColumns.html.width) to 'col-sm-'+currentColumns.length?
        //console.log(newColumns);
        //_.min(_.pluck(obj, "val"))
        /*_.each(this.page_columns, function (column) {
         console.log(column.rank);
         });
         */
        //if this.page_columns.length > evt.target.value{
        // confirm: you are about to delete ( var change = this.page_columns.length - evt.target.value) columns
        // if true Meteor.call('updatePage....,change, func...
        // }
        //._sortBy(this.pageColumns, rank) this should already been done

    },
    'change .js-cheatsheet_width': function (evt, tmpl) {
        Meteor.call('updatePageformat', this._id, evt.target.value,function (error, result) {
        });
        console.log(evt.target.value)

    }
});

Template.cheatsheetProperties.helpers({
    cheatsheet: function(){
       var cheatsheet = Session.get("cheatsheet");
        return cheatsheet;
    },
    editingCheatsheetName:function(){
        console.log('bongo', this._id);
        return Session.equals('editing_cheatsheetname',this._id);
    },
    templates:function(){
        var templates = Cheatsheets.find({type:"template"});
        var templatesOld = [{_id:1, name:'template 1 column'},{_id:2, name:'template 2 column'},{_id:3, name:'template 3 column'},{_id:4, name:'template 4 column'}]

        return templates;
    },
    isSelected: function (name ) {
        if(name == 'template 1'){
            return "selected";
        }
    },
    columnNumbers: function (name ) {
        var columnArray = _.range(1,13);
        //var columnArray = [1,2,3,4,5,6,7,8,9,10,11,12]
        return columnArray;
    },
    pageFormat: function (name ) {
        var selectionArray = ["computer", "iphone", "ipad", "hybrid", "pdf-print"]
        var valueArray = ["computer-width", "smartphone-width", "ipad-width", "hybrid-width", "a4-printing-width"]
        var valueArray = [  {'value':"responsive-width", 'selection':'responsive(adapts to screen-size, recommended)'},
            {'value':"computer-width", 'selection':'computer'},
            {'value':"smartphone-width", 'selection':'smartphone'},
            {'value':"ipad-width", 'selection':'ipad'},
            {'value':"hybrid-width", 'selection':'hybrid'},
            {'value':"a4-printing-width", 'selection':'A4 printing'},
        ]
        var contentObject = {'selection':selectionArray, 'value':valueArray}
        return valueArray;
    }
})


Template.cheatsheetProperties.rendered = function(){
    //var datas = this.data._id;
};

var newMongoId = function(){
    var nid = new Mongo.ObjectID;
    var nid_str = nid._str;

    return nid_str;
}